/**
 * screen-script.js
 */
define(
	
	[
		 'jquery'
		,'backbone'
		,'ckeditor'
		,'ckfinder'
	],  
	
	function($, Backbone, ckEditor, CKFinder) {
		
		/**
		 * Define foundation model view
		 */
		var PageView = Backbone.View.extend({
			
			initialize: function() {
				trace("got this far");
				var editor = CKEDITOR.replace('body',{
				    filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
				    filebrowserUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files'
				});

				$("#title").bind("keyup",this.updateSlug);
				$("#title").bind("change",this.updateSlug);

				$(".bReplaceThumb").bind("click", this.replaceImageThumb);
				$(".bReplace").bind("click", this.replaceImage);

				$(".bRemoveThumb").bind("click", this.removeImageThumb);
				$(".bRemove").bind("click", this.removeImage);

				$("#gallerySelect").bind("click", this.selectGallery);
				$("#gallerySelect").bind("change", this.selectGallery);
			}

			,updateSlug: function(e) {
				var newslug = slug($("#title").val());
				$('#slug').val(newslug);
			}

			,replaceImageThumb: function(e) {
				e.preventDefault();
				$("#imgPreviewThumb").addClass("hidden");
				$("#imgUploadThumb").removeClass("hidden");
				$('html,body').animate({scrollTop: 0},'slow');
			}

			,replaceImage: function(e) {
				e.preventDefault();
				$("#imgPreview").addClass("hidden");
				$("#imgUpload").removeClass("hidden");
				$('html,body').animate({scrollTop: 0},'slow');
				
			}

			,removeImageThumb: function(e) {
				e.preventDefault();
				$("#imgPreviewThumb").addClass("hidden");
				$("#imgUploadThumb").removeClass("hidden");
				$('html,body').animate({scrollTop: 0},'slow');
				$("#clear_thumb").val("1");
			}

			,removeImage: function(e) {
				e.preventDefault();
				$("#imgPreview").addClass("hidden");
				$("#imgUpload").removeClass("hidden");
				$('html,body').animate({scrollTop: 0},'slow');
				$("#clear_image").val("1");
			}

			,selectGallery: function(e) {
				//e.preventDefault();
				if ($(this).val() == '0') {
					$("#galleryPreview").addClass("hidden");
				} else {
					$("#galleryPreview").removeClass("hidden");
				}
			}
		});
		
		return PageView;
	}
);