/**
 * screen-script.js
 */
define(
	
	[
		 'jquery'
		,'backbone'
		,'ckeditor'
	],  
	
	function($, Backbone, ckEditor) {
		
		/**
		 * Define foundation model view
		 */
		var PageView = Backbone.View.extend({
			
			initialize: function() {
				CKEDITOR.replace('body');

				$("#title").bind("keyup",this.updateSlug);
				$("#title").bind("change",this.updateSlug);

				$(".bReplaceThumb").bind("click", this.replaceImageThumb);
				$(".bReplace").bind("click", this.replaceImage);
			}

			,updateSlug: function(e) {
				var newslug = slug($("#title").val());
				$('#slug').val(newslug);
			}

			,replaceImageThumb: function(e) {
				e.preventDefault();
				$("#imgPreviewThumb").addClass("hidden");
				$("#imgUploadThumb").removeClass("hidden");
				$('html,body').animate({scrollTop: 0},'slow');
			}

			,replaceImage: function(e) {
				e.preventDefault();
				$("#imgPreview").addClass("hidden");
				$("#imgUpload").removeClass("hidden");
				$('html,body').animate({scrollTop: 0},'slow');
			}
		});
		
		return PageView;
	}
);