/**
 * main.js
 * contents of main.js with extra configuration
 */
require.config({
	
	baseUrl: "/js/",
	
	//urlArgs: "bust=" + (new Date()).getTime(),	// halt caching of .js files for development
	
	waitSeconds: 0, 							// disable script loading timeout 
    
	paths: {
		
		/**
		 * Libraries
		 */
         'jquery'		: 'libs/jquery/jquery-2.2.3.min'
        ,'preload'		: 'libs/preload/image-preload.min'
        ,'text'		 	: 'libs/requirejs/plugins/text'
		
		/**
		 * templates
		 */
		,'carSection'	: 'templates/carSection.htm'		
		,'carOrb'		: 'templates/carOrb.htm'		
		
		/* Tweenlite Logic */
        ,'tweenlite'			: 'libs/gsap/TweenLite.min'
		,'cssPlugin'			: 'libs/gsap/plugins/CSSPlugin.min'
		,'scrolltoPlugin'		: 'libs/gsap/plugins/ScrollToPlugin.min'

		/* Unison */
		,'unison'		: 'libs/unison/unison.min'

		/* Triggered animations */
		,'animateit'			: 'libs/animate-it/css3-animate-it'
		

		/* Video player engine */
		,'jplayer'				: 'libs/jplayer/jquery.jplayer.min'
		,'mediaplayer'			: 'libs/jplayer/jplayer.easydeploy.min'

		/* Responsive Image maps */
		,'rwdImageMaps'			: 'libs/responsive-image-maps/jquery.rwdImageMaps.min'

		/* hammer */
		,'hammer'		: 'libs/hammer/hammer.min'
		,'carousel'		: 'main/sections/partials/carousel.min'
		,'equaliser'	: 'main/equaliser.min'
		
		/* fancybox */
		,'fancybox'				: 'libs/fancybox/jquery.fancybox.pack'

		/**
		 * Application Logic
		 */
		,'common'		: 'main/common.min'
		,'init'			: 'main/init.min'
		,'unison-model'	: 'main/unison-model.min'
    },

	/**
	 * Use shim for non AMD (Asynchronous Module Definition) compatible scripts
	 */
	shim: {        

        'unison': {
            exports : 'Unison'
        },

        'animateit': {
         	deps 	: ['jquery']
            ,exports : 'animateit'
        },

        'tweenlite': {
            exports : 'TweenLite'
        },
		
		'cssPlugin': {
			 deps	 : ['tweenlite']
            ,exports : 'CSSPlugin'
        },
		
		'scrolltoPlugin': {
			 deps	 : ['tweenlite']
            ,exports : 'ScrollToPlugin'
        },

        'rwdImageMaps': {
			 deps	 : ['jquery']
            ,exports : 'rwdImageMaps'
        },

        'mediaplayer': {
        	deps	 : ['jquery','jplayer']
        	,exports : 'MediaPlayer'
        },

        'fancybox': {
         	deps 	: ['jquery']
            ,exports : 'FancyBox'
        },

        'fancybox-media': {
         	deps 	: ['jquery','fancybox']
            ,exports : 'FancyBoxMedia'
        },
    }
});

/**
 * Load the init module
 */
require(['init'],
		
	function(App) {
		window._app	= new App();
	}
);