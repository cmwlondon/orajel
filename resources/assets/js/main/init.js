/**
 * init.js
 */
define(
	[
		 'jquery'
		,'common'
		,'unison-model'
		,'equaliser'
		,'tweenlite'
		,'cssPlugin'
		,'animateit'
		,(pageViewJS === "" ? {} : pageViewJS)
	],
	
	function($, common, UnisonModel, Equaliser, TweenLite, cssPlugin, AnimateIt, PageView) {
		
		function me() {
			this.initialize();
		};
		
		me.prototype = {

			mobileMenu: 'close',
			cookieDisclaimer:false,
			cookieName: 'OrajelDisclaimerCookie',

			initialize: function() {
				
				if ($("#noIE").css("position") != 'fixed') {
					trace("here");


					$.force_appear();

					window.UnisonModel = new UnisonModel();
					
					/**
					 * Initialise page actions if available
					 */
					if(typeof PageView !== "undefined") {
						window.pageView = new PageView();
					}

					this.initMobileMenu();

					window.Equaliser = new Equaliser();

					// $(".retailer").bind("click",function(e) {
					// 	var label = $(e.currentTarget).data("label");
					// 	TrackEventGA("retailer","click",label,1);
					// });

					$(".cookie-disclaimer .btn-close").bind("click",() => {this.closeCookieDisclaimer()});
					this.cookieDisclaimer = !this.checkCookie();
					this.updateDisclaimerStyles();
					window.clearCookie = () => {
						this.clearCookie();
					}
				}
			}

			,initMobileMenu: function() {
				TweenLite.to($("#mobile-white-out"), 0, {opacity:0});
				$(".burger").bind("click", {parent:this}, function(e) {
					if (e.data.parent.mobileMenu == 'open') {
						e.data.parent.toggleMenu("close");
					} else {
						e.data.parent.toggleMenu("open");
					}
				});

				$("#mobile-white-out").bind("click", {parent:this}, function(e) {
					if (e.data.parent.mobileMenu == 'open') {
						e.data.parent.toggleMenu("close");
					}					
				})

				$(".slide-menu .close").bind("click", {parent:this}, function(e) {
					if (e.data.parent.mobileMenu == 'open') {
						e.data.parent.toggleMenu("close");
					}					
				});

				window.addEventListener('unichange', this.bpChange.bind(this), false);
			}

			,toggleMenu: function(state) {
				this.mobileMenu = state;
				if (state == 'open') {
					TweenLite.to($("#wrapper"), 0.3, {css:{marginLeft:"-320px"}, ease: Quad.easeOut});
					TweenLite.to($(".slide-menu"), 0.3, {css:{right:"15px"}, ease: Quad.easeOut});
					TweenLite.to($("#mobile-white-out"), 0.3, {opacity:0.75, onStart: function() {$("#mobile-white-out").removeClass("hidden")}});
				} else {
					TweenLite.to($("#wrapper"), 0.3, {css:{marginLeft:"0"}, ease: Quad.easeOut});
					TweenLite.to($(".slide-menu"), 0.3, {css:{right:(15-320)+"px"}, ease: Quad.easeOut});
					TweenLite.to($("#mobile-white-out"), 0.3, {opacity:0, onComplete: function() {$("#mobile-white-out").addClass("hidden")}});
				}				
			}

			,bpChange: function() {
				//trace("bpChange: " + window.UnisonModel.getWidth());
				if (window.UnisonModel.getWidth() >= 769) {
					if (this.mobileMenu == 'open') {
						this.toggleMenu("close");
					}
				}
			},
			updateDisclaimerStyles: function() {
				if (this.cookieDisclaimer) {
					$(".cookie-disclaimer").addClass("shown");
				} else {
					$(".cookie-disclaimer").removeClass("shown");
				}
			},
			closeCookieDisclaimer: function(e) {
				this.cookieDisclaimer = false;
				this.updateDisclaimerStyles();
				this.setCookie();
			},
			setCookie: function() {
				var expiresDays = 1/24;
				var d = new Date();
				d.setTime(d.getTime() + (expiresDays*24*60*60*1000));
				var expires = "expires=" + d.toGMTString();
				document.cookie = this.cookieName + "=policy;"+expires+";path=/";
			},
			getCookie: function() {
				var name = this.cookieName + "=";
				var decodedCookie = decodeURIComponent(document.cookie);
				var ca = decodedCookie.split(';');
				for(var i = 0; i < ca.length; i++) {
				var c = ca[i];
				while (c.charAt(0) == ' ')
				  c = c.substring(1);
				if (c.indexOf(name) == 0)
				  return c.substring(name.length, c.length);
				}
				return "";
			},
			checkCookie: function() {
				var user = this.getCookie();
				if (user != "")
					return true;
				else
					return false;
			},
			clearCookie: function() {
				var d = new Date();
				var expires = "expires=" + d.toGMTString();
				document.cookie = this.cookieName + "=policy;"+expires+";path=/";
				this.cookieDisclaimer = true;
				this.updateDisclaimerStyles();
			}
		};
		
		return me;
	}
)