/**
 * equaliser.js
 */
define(
	[	
		 'jquery'
	],  
	
	function($) {
			
		/**
		 * Define foundation model view
		 */
		var Equaliser = function() {		
			this.constructor();
		}

		Equaliser.prototype = {
			
			constructor: function() {
				this.fixHeights();

				$(window).bind("resize", this.fixHeights);

				//window.addEventListener('unichange', this.fixHeights.bind(this), false);			
			},

			fixHeights: function() {
				var el = $(".eq");
				if (UnisonModel.getWidth() < 768) {
					el.each(function() {
						$(this).height("auto");
					});
				} else {
					var max = 0;
					el.each(function() {
					 	max = Math.max(max,$(this).find(".eq-inner").height());
					});

					el.each(function() {
						$(this).height(max);
					});
				}

				
			}
		};
		
		return Equaliser;
	}
);