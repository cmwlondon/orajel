/**
 * home.js
 */
 /*
define(['jquery', 'carousel', 'rwdImageMaps'],
	
	function($, Carousel) {
		
		function me() {
			this.initialize();
		};
		
		me.prototype = {
			initialize: function() {
				
				window.homeCarousel = new Carousel({
					 'container'	: 'div.carousel-inner'
					,'orbContainer' : 'ul.carousel-control'
					,'imgDir'	 	: '/images/home/carousel/'
					,'imgArray'	 : [
						{ 
							 'imageLarge' 		: 'carousel-1-large.jpg'
							,'imageLargeMapId'	: 'Image-Maps-Com-image-maps-2016-05-16-071607'
							,'useMapLarge'		: '#image-maps-2016-05-16-071607'
							 
							,'imageSmall' 		: 'carousel-1-small.jpg'
							,'imageSmallMapId'	: 'Image-Maps-Com-image-maps-2016-05-16-071000'
							,'useMapSmall'		: '#image-maps-2016-05-16-071000'
							
							,'link'			: '/finger-fighters'
							,'linkText'		: 'FIND OUT MORE'
							,'target'		: '_self'
						},
						{ 
							 'imageLarge'		: 'carousel-2-large.jpg'
							,'imageLargeMapId'	: 'Image-Maps-Com-image-maps-2016-05-16-055730'
							,'useMapLarge'		: '#image-maps-2016-05-16-055730'
							
							,'imageSmall'		: 'carousel-2-small.jpg'
							,'imageSmallMapId'	: 'Image-Maps-Com-image-maps-2016-05-16-053907'
							,'useMapSmall'		: '#image-maps-2016-05-16-053907'
							
							,'link'				: '/where-to-buy'
							,'target'			: '_self'
						}, 
						{ 
							 'imageSmall'		: 'carousel-3-small.jpg'
							,'imageSmallMapId'	: 'Image-Maps-Com-image-maps-2016-05-16-054115'
							,'useMapSmall'		: '#image-maps-2016-05-16-054115'
							 
							,'link'			: '/where-to-buy'
							,'target'		: '_self'
						}, 
					]
					,'callback': function() {
						$('img[usemap]').rwdImageMaps();
					}
				});
				
			},
		};
		
		return me;
	}
);
*/