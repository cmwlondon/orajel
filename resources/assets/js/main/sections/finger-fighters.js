/**
 * home.js
 */
define(['jquery', 'fancybox', 'rwdImageMaps','jplayer','mediaplayer','tweenlite'
		,'cssPlugin'],
	
	function($, FancyBox, rwdImageMaps, jplayer, mediaplayer, TweenLite, cssPlugin) {
		
		function me() {
			this.initialize();
		};
		
		me.prototype = {
			videos: {
				'ninja':{'file' : 'collective-ninja-700x500_1.mp4', 'ratio' : 'normal'},
				'hero':{'file' : 'collective-superhero-700x500_1.mp4', 'ratio' : 'normal'},
				'wrestler':{'file' : 'collective-wrestler-700x500_1.mp4', 'ratio' : 'normal'},
				'bts':{'file' : 'orajel-behind-the-scenes.mp4', 'ratio' : 'wide'}
			},

			initialize: function() {
				
				$('img[usemap]').rwdImageMaps();
				
				var parent = this;

				TweenLite.to($(".overlay"), 0, {opacity:0});

				$(".fancybox").bind("mouseover", function(e) {
					var id = $(this).data("id");
					TweenLite.to($('.overlay[data-id="'+id+'"]'), 0.3, {opacity:1});
				});

				$(".fancybox").bind("mouseleave", function(e) {
					var id = $(this).data("id");
					TweenLite.to($('.overlay[data-id="'+id+'"]'), 0.3, {opacity:0});
				})

				$(".fancybox").bind("click", function(e) {
					e.preventDefault();
					var videoFile = parent.videos[$(this).data("id")];

					$.fancybox({
						'href' : $(this).attr("href"),
						padding : 0,
    					scrolling : 'no',

    					beforeShow : function() {
    						window.pageView.playVideo(videoFile,'');
    					},
    					afterClose: function() {
    						$("#video").empty();
    					}
					},
					{
						//width:'700px',
						//height: '500px',
						//aspectRatio: true,
						helpers : {
							overlay: {
								locked: false
							}
						}
					});
					return false;
				});
			},

			playVideo: function(video,placeholder) {
				
				$("#video").empty();

				$("#video").append($('<div id="contentPlayer" class="mediaPlayer"><div id="contentContainer" class="Player hidden"></div></div>'));

				if (video.ratio == 'wide') {
					$(".fancybox-wrap").addClass("wide");
				} else {
					$(".fancybox-wrap").removeClass("wide");
				}

				var settings = {
					media: {
						m4v: "https://s3-eu-west-1.amazonaws.com/uk.co.orajel/"+video.file
						//poster: "https://s3-eu-west-1.amazonaws.com/uk.co.orajel/"+placeholder
					},

					size: {
						width: '100%',
					 	height: '100%'
					},

					// These go directly to jPlayer object, allowing you to rewrite any player setting
					loadstart: function() {
						
					},

					playing: function() {
						$(".Player").removeClass("hidden");
					},

					ended: function() {
						
					},
					autoplay: true
				};
				$('#contentPlayer').mediaPlayer(settings);
			}
		};
		
		return me;
	}
);