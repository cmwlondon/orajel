/**
 * carousel.js
 * uses ecmascript 6 syntax
 */
define(['jquery', 'text!carSection', 'text!carOrb', 'hammer', 'preload'],
	
	function($, contentTemp, orbTemp, Hammer) {
		
		/**
		 * Hammer swipe actions
		 */
        var reqAnimationFrame = (() => {
            return window[Hammer.prefixed(window, "requestAnimationFrame")] || function(callback) {
                setTimeout(callback, 1000 / 60);
            }
        })();
		
        var dirProp = direction => (direction & Hammer.DIRECTION_HORIZONTAL) ? true : false;
		
		class Carousel {
			
			constructor(obj) {
				
				// assign values and keys of object argument to this scope
				Object.assign(this, obj);
				
				this.container		 = document.querySelector(this.container);
				this.orbContainer	 = document.querySelector(this.orbContainer);
				this.direction		 = Hammer.DIRECTION_HORIZONTAL;
				this.containerSize	 = ( dirProp(this.direction) ? this.container['offsetWidth'] : 0 );
				this.contentTemp	 = contentTemp;
				this.orbTemp	 	 = orbTemp;
				this.currentIndex	 = 0;
				this.scrollThreshold = 30;
				this.cycle			 = null;
				this.imageMapFired	 = false;
				
				// Add event dispatcher for carousel load event  
				this.CarouselDone = document.createEvent("Event");
				this.CarouselDone.initEvent("carouselDone", true, true);
				
				(this.containerSize < 769 ? this.type = 'small' : this.type = 'large' );
				
				this.initialize();
			};

			/**
			 * Preload images
			 */
			initialize() {
				
				$(this.imgArray).preload(this.imgDir, () => {
					
					this.buildCarousel();
					
					/**
					 * Initialise hammer Managers
					 */
					if(this.supportsTransitions() && this.containerSize < 769) {
						
						let hammerOptions = {
							 dragLockToAxis		: true
							,dragBlockVertical	: true
							,preventDefault		: true
						}
						
						this.hammer	= new Hammer.Manager(this.container, hammerOptions);

						/**
						 * Set/initialise pan options
						 */
						let panOptions = {
							 direction: Hammer.DIRECTION_HORIZONTAL
							,threshold: 1
						}
						
						this.hammer.add(new Hammer.Pan(panOptions));
						this.hammer.on("panstart panmove panend pancancel", Hammer.bindFn(this.onPan, this));
					}
					
					/**
					 * Throttle window resize event listener
					 * resize banner containers
					 */
					var throttle; 
					$(window).resize(() => {
						
						clearInterval(throttle);
						
						this.imageMapFired = false;
						
						throttle = setTimeout(() => {
						
							this.containerSize = this.container['offsetWidth'];
							
							this.buildCarousel();
							
						}, 300);
					});
				});
			};
			
			/**
			 * build carousel
			 */
			setStructure(type) {
				
				let that			= this;
				let $container		= $(this.container);
				let $orbContainer	= $(this.orbContainer);
				
				$container.empty();
				$orbContainer.empty();
				
				let buffer = $.map(this.imgArray, (value, index) => {
					
					var valueImage = (this.type === 'large' ? value.imageLarge : value.imageSmall );
					
					if(typeof valueImage !== 'undefined') {
						
						let $content = $(this.contentTemp).clone();
						
						$orbContainer.append($('<li/>', {
							 'data-id': index
							,'class': (index===0 ? 'active' : '')
						}));
						
						$content.find('img').attr('src', this.imgDir + valueImage);
						$content.find('img').attr('onmousedown', 'return false');
						
						if(this.containerSize > 769) {
							
							if(typeof value.imageLargeMapId !== 'undefined')
								$content.find('img').attr('id', value.imageLargeMapId);
							
							if(typeof value.useMapLarge !== 'undefined')
								$content.find('img').attr('usemap', value.useMapLarge);
						}
						else {
								
							if(typeof value.imageSmallMapId !== 'undefined')
								$content.find('img').attr('id', value.imageSmallMapId);
							
							if(typeof value.useMapSmall !== 'undefined')
								$content.find('img').attr('usemap', value.useMapSmall);
							
						}
						
						//$content.find('img').attr('onmouseup', 'return this.onclick() || this.click()');
						$content.find('a').attr('href', value.link);
						$content.find('a').attr('target', value.target);
						
						if(typeof value.linkText !== 'undefined')
							$content.find('a').text(value.linkText);
						
						return ($content.get());
					}
					
				});
				
				$container.append(buffer);
				this.containerSize = (dirProp(this.direction) ? this.container['offsetWidth'] : 0 );
				(this.containerSize < 769 ? this.type = 'small' : this.type = 'large' );
				this.panes = Array.prototype.slice.call(this.container.children, 0);
				this.hightlightOrb(this.currentIndex);
				
				//window.dispatchEvent(that.CarouselDone);
				
				this.initialiseHammer();
			};
			
			buildCarousel() {
				
				if(parseInt(UnisonModel.getWidth()) < 769) {

					if(this.type === 'large') {
						
						this.type = 'small';
						this.currentIndex = 0;
						this.setPadding(this.type);
						this.setStructure(this.type);
					}
					else {
						
						this.setPadding('small');
						this.setStructure(this.type);
					}
				}
				else if(parseInt(UnisonModel.getWidth()) >= 769) {
					
					if(this.type === 'small') {
						
						this.type = 'large';
						this.currentIndex = 0;
						this.setPadding(this.type);
						this.setStructure(this.type);
					}
					else {
						this.setPadding('large');
						this.setStructure(this.type);
					}
				}

				$(this.container).parent().find(".shim").remove();
			};
			
			setPadding(t) {
				
				if(t === 'small') {
					
					$(this.container).parent().removeClass('largePadding');								
					$(this.container).parent().addClass('smallPadding');
				}	
				else {
					
					$(this.container).parent().removeClass('smallPadding');								
					$(this.container).parent().addClass('largePadding');
				}
			};
			
			initialiseHammer() {
				
				/**
				 * Set handler for orb clicks
				 */
				$('.'+this.orbContainer.className + ' > li').off();
				$('.'+this.orbContainer.className + ' > li').on('click', (e) => {
					
					let $eTarget = $(e.target);
					let index = $eTarget.data('id');
                    
					this.hightlightOrb(index);
					this.show(index, 0, true);
					this.cycleNext();
				});
				
				clearInterval(this.cycle);
				this.cycleNext();
				this.show(this.currentIndex);
				
				if(!this.imageMapFired) {
					this.callback();
					this.imageMapFired = true;
				}
			};
			
			/**
             * show a pane
             * @param {int} showIndex
             * @param {int} [percent] percentage visible
             * @param {Boolean} [animate]
             */
            show(showIndex, percent, animate) {
				
				showIndex = Math.max(0, Math.min(showIndex, this.panes.length - 1));
				
                percent = percent || 0;
				
                let className = this.container.className;
				
                if(animate) {
                    if(className.indexOf('animate') === -1) {
                        this.container.className += ' animate';
                    }
                }
				else {
                    if(className.indexOf('animate') !== -1) {
                        this.container.className = this.container.className.replace('animate', '').trim();
                    }
                }
				
				var paneIndex, pos, translate;
				
				for (paneIndex = 0; paneIndex < this.panes.length; paneIndex++) {
						
					pos	= (this.containerSize / 100) * (((paneIndex - showIndex) * 100) + percent);
					
					translate	= 'translate(' + pos + 'px, 0)';
					this.panes[paneIndex].style.transform		= translate;
					this.panes[paneIndex].style.mozTransform	= translate;
					this.panes[paneIndex].style.webkitTransform = translate;
					this.panes[paneIndex].style.msTransform		= translate;
				}		
				
				this.currentIndex = showIndex;
            };
			
			/**
             * handle pan
             * @param {Object} ev
             */
            onPan(ev) {
				
				let  delta	 = (dirProp(this.direction) ? ev.deltaX : 0 )
					,percent = (100 / this.containerSize) * delta
					,animate = false
					,that 	 = this;

				if (ev.type == 'panend' || ev.type == 'pancancel') {
					
					if (Math.abs(percent) > this.scrollThreshold && ev.type == 'panend') {
						
						this.currentIndex += (percent < 0) ? 1 : -1;

						this.hightlightOrb(this.currentIndex);
						
						this.cycleNext();
					}
					
					percent	= 0;
					animate = true;
				}
				
				reqAnimationFrame(this.show.bind(this, this.currentIndex, percent, animate));
            };
			
			/**
			 * Highlight orbs depending
			 * 
			 * @param {int} ind | number to test for change and pass 
			 */
			hightlightOrb(ind) {
				
				var i = ind;
				
				if(i > (this.panes.length -1))
					i = (this.panes.length -1);
				else if(i < 0)
					i = 0;
				
				$('.'+this.orbContainer.className + ' > li').removeClass('active');

				$('.'+this.orbContainer.className + ' > li').each((index, value) => {
					
					if(ind === -1)
						ind = 0;
					
					if(this.type === 'large') {
						if(ind === 2)
							ind = 1;
					}
					else {
						if(ind === 3)
							ind = 2;
					}
					
					if($(value).data('id') === ind)
						$(value).addClass('active');
				});
			};
			
			cycleNext() {
				
				clearInterval(this.cycle);
				
				this.cycle = setInterval(() => {
					
					if(this.type === 'large') {
						if(this.currentIndex >= 2)
							this.currentIndex = 0;
					}
					else {
						if(this.currentIndex > 2)
							this.currentIndex = 0;
					}
					
					let percent	= 0;
					let animate = false;
					
					this.hightlightOrb(this.currentIndex);

					this.show(this.currentIndex, 0, true);

					this.currentIndex = this.currentIndex +1;
					
				}, 5 * 1000);
			};
			
			/**
			 * Check to see if scroll falls between supplied parameters
			 *@param {int} top
			 *@param {int} bottom
			 */
			checkBounds(top, bottom) {
				
				return (($(window).scrollTop()-$(this.container).offset().top) > top && ($(window).scrollTop()-$(this.container).offset().top) < bottom);
			};
			
			supportsTransitions() {
				let b = document.body || document.documentElement,
					s = b.style,
					p = 'transition';

				if (typeof s[p] == 'string') { return true; }

				// Tests for vendor specific prop
				let v = ['Moz', 'webkit', 'Webkit', 'Khtml', 'O', 'ms'];
				p = p.charAt(0).toUpperCase() + p.substr(1);

				for (let i=0; i<v.length; i++) {
					if (typeof s[v[i] + p] == 'string') { return true; }
				}

				return false;
			}
		};
		
		return Carousel;
	}
);