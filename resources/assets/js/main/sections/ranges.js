/**
 * home.js
 */
define(['jquery', 'fancybox'],
	
	function($, FancyBox) {
		
		function me() {
			this.initialize();
		};
		
		me.prototype = {
			initialize: function() {
				$(".fancybox").bind("click", function(e) {
					e.preventDefault();
					$.fancybox({
						'href' : $(this).attr("href"),
						helpers : {
							overlay: {
								locked: false
							}
						},
						'wrapCSS' : 'useageClass'
					});
					return false;
				});
			},
		};
		
		return me;
	}
);