@extends('main.layouts.main')


@section('header')
	
@endsection


@section('content')
	<div class="row full head">
		
		<div class="w100 ff inner">
			<img src="/images/finger-fighters/header.jpg" class="w100 ff" usemap="#ToothPainMap"/>
			<img src="/images/finger-fighters/ninja-hilight.png" class="overlay" usemap="#ToothPainMap" data-id="ninja"/>
			<img src="/images/finger-fighters/wrestler-hilight.png" class="overlay" usemap="#ToothPainMap" data-id="wrestler"/>
			<img src="/images/finger-fighters/hero-hilight.png" class="overlay" usemap="#ToothPainMap" data-id="hero"/>
		</div>

		<map name="ToothPainMap">
		  <area shape="poly" coords="205,836,154,622,128,605,126,587,111,576,90,585,79,577,85,559,22,502,20,485,32,476,56,487,118,534,123,518,128,495,93,398,94,357,102,325,100,299,122,282,146,250,161,265,183,304,218,327,242,336,260,380,294,461,331,486,374,398,389,393,387,453,366,498,379,506,377,516,353,513,342,542,346,551,338,569,376,689,384,713,395,765,394,835" href="#video" data-id="ninja" class="fancybox">
		  <area shape="poly" coords="398,835,399,739,401,663,384,528,388,460,393,389,389,363,383,338,389,281,408,252,426,241,445,203,468,181,477,205,491,216,495,239,523,261,538,283,548,313,548,352,544,364,552,415,562,428,571,476,578,514,571,580,575,661,577,735,583,756,575,770,579,836" href="#video" data-id="wrestler" class="fancybox">
		  <area shape="poly" coords="581,837,584,756,579,736,575,635,597,604,609,594,588,569,586,537,579,513,583,474,616,426,644,406,684,398,712,388,720,357,727,332,740,285,755,260,773,248,776,239,792,233,803,219,818,218,839,195,839,229,847,261,854,284,859,314,859,362,845,445,857,471,863,500,850,530,853,537,873,533,901,528,928,541,962,555,986,559,1015,558,1011,585,1020,603,1042,626,1067,640,1105,649,1123,665,1132,702,1124,741,1105,768,1091,780,1078,747,1057,724,1036,712,1013,720,981,732,955,739,936,732,909,721,881,683,864,663,847,657,831,670,819,688,808,696,803,760,797,798,797,823,779,835" href="#video" data-id="hero" class="fancybox">
		  <area shape="rect" coords="968,893,1199,953" href="#where-to-buy">
		</map>
	</div>

	<div class="block-black spaced">
		<a name="videos"></a>
		<h3>Meet the Orajel Finger Fighters</h3>
	</div>

	<div class="row nopad toothpainVideos">
		<div class="columns span-6 sm-lg-12">
			<img src="/images/finger-fighters/ninja-background.jpg" class="bg"/>
			<div class="text">
				<a href="#video" data-id="ninja" class="fancybox">Play Video</a>
			</div>
		</div>
		<div class="columns span-6 sm-lg-12">
			<img src="/images/finger-fighters/superheroine-background.jpg" class="bg"/>
			<div class="text">
				<a href="#video" data-id="hero" class="fancybox">Play Video</a>
			</div>
		</div>
		<div class="columns span-6 sm-lg-12">
			<img src="/images/finger-fighters/wrestler-background.jpg" class="bg"/>
			<div class="text">
				<a href="#video" data-id="wrestler" class="fancybox">Play Video</a>
			</div>
		</div>
		<div class="columns span-6 sm-lg-12">
			<img src="/images/finger-fighters/behind-the-scenes.jpg" class="bg"/>
			<div class="text middle">
				<a href="#video" data-id="bts" class="fancybox">Watch the making of video</a>
			</div>
		</div>
	</div>

	<div class="block-red spaced">
		<a name="mouth-gel"></a>
		<h3>No.1 selling toothache gel*</h3>
	</div>

	<div class="block-grey">
		<div class="row nopad info mt3 animatedParent animateOnce" data-sequence="100">
			<div class="columns span-6 md-10 md-before-1 md-after-1 sm-10 sm-before-1 sm-after-1">
				@include('main.range.partials._dental_gel')
			</div>
			<div class="columns span-6 md-12 sm-12 tac mb4">
				<img src="/images/ranges/dental-gel.png" class="pack animated growIn" data-id="5" alt="Orajel&trade; Dental Gel"/>
				<a href="#where-to-buy" class="wtb btnWtb btnRed">Where to buy</a>
			</div>
		</div>
	</div>

	<div class="block-yellow spaced">
		<a name="mouth-gel"></a>
		<h3>Rapid Relief of acute Toothache</h3>
	</div>

	<div class="block-grey">
		<div class="row nopad info mt3 animatedParent animateOnce" data-sequence="100">
			<div class="columns span-6 md-10 md-before-1 md-after-1 sm-10 sm-before-1 sm-after-1">
				@include('main.range.partials._extra_strength')
			</div>
			<div class="columns span-6 md-12 sm-12 tac mb4">
				<img src="/images/ranges/extra-strength.png" class="pack animated growIn" data-id="5" alt="Orajel&trade; Extra Strength"/>
				<a href="#where-to-buy" class="wtb btnWtb btnRed">Where to buy</a>
			</div>
		</div>
	</div>

	<div class="block-blue spaced">
		<a name="mouth-gel"></a>
		<h3>For effective relief of mouth ulcer &amp; denture pain</h3>
	</div>

	<div class="block-grey">
		<div class="row nopad info mt3 animatedParent animateOnce" data-sequence="100">
			<div class="columns span-6 md-10 md-before-1 md-after-1 sm-10 sm-before-1 sm-after-1">
				@include('main.range.partials._mouth_gel')
			</div>
			<div class="columns span-6 md-12 sm-12 tac mb4">
				<img src="/images/ranges/mouth-gel.png" class="pack animated growIn" data-id="5" alt="Orajel&trade; Mouth Gel"/>
				<a href="#where-to-buy" class="wtb btnWtb btnBlue">Where to buy</a>
			</div>
		</div>
	</div>

	<div class="block-black spaced">
		<a name="where-to-buy"></a>
		<h3>Where to buy</h3>
	</div>

	<div class="block-grey where-to-buy">
		<div class="row">
			<div class="columns span-12 intro">
				<p class="mt2">Orajel&trade; is available from Boots, Asda, Amazon, Sainsbury&rsquo;s, Superdrug, Lloyds Pharmacy and also widely available from a range of independent pharmacies.</p>
	        </div>
		</div>

		@include('main.layouts.partials._wtb')

	</div>
	<div id="video"></div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
