<div class="row nopad info animatedParent animateOnce" data-sequence="100">
	<div class="columns span-4 md-6 sm-6">
		<a href="http://www.boots.com/webapp/wcs/stores/servlet/EndecaSearchListerView?storeId=10052&langId=-1&catalogId=11051&stReq=1&searchTerm=orajel&newDepSearch=&x=0&y=0#container" target="_blank" class="retailer" data-label="boots" onClick="ga('send', 'event', { eventCategory: 'Link', eventAction: 'Click Link', eventLabel: 'Boots'});"><img src="/images/where-to-buy/boots.svg" class="logo animated fadeIn" data-id="1"/></a>
	</div>
	<div class="columns span-4 md-6 sm-6">
		<a href="http://groceries.asda.com/asda-webstore/landing/home.shtml?cmpid=ahc-_-ghs-_-asdacom-dsk-_-hp-_-sub-gro-title&referrer=cookiesDetecting#/search/orajel" target="_blank" class="retailer" data-label="asda" onClick="ga('send', 'event', { eventCategory: 'Link', eventAction: 'Click Link', eventLabel: 'Asda'});"><img src="/images/where-to-buy/asda.svg" class="logo animated fadeIn" data-id="2"/></a>
	</div>
	{{--<div class="columns span-4 md-6 sm-6">
		<a href="http://www.tesco.com/groceries/Product/Details/?id=256105703" target="_blank" class="retailer" data-label="tesco" onClick="ga('send', 'event', { eventCategory: 'Link', eventAction: 'Click Link', eventLabel: 'Tesco'});"><img src="/images/where-to-buy/tesco.svg" class="logo animated fadeIn" data-id="3"/></a>
	</div>--}}
	<div class="columns span-4 md-6 sm-6">
		<a href="https://www.amazon.co.uk/s/ref=nb_sb_noss_2?url=search-alias%3Ddrugstore&field-keywords=orajel" target="_blank" class="retailer" data-label="amazon" onClick="ga('send', 'event', { eventCategory: 'Link', eventAction: 'Click Link', eventLabel: 'Amazon'});"><img src="/images/where-to-buy/amazon.svg" class="logo animated fadeIn" data-id="3"/></a>
	</div>
	<div class="columns span-4 md-6 sm-6">
		<a href="https://www.sainsburys.co.uk/shop/gb/groceries/orajel-dental-gel" target="_blank" class="retailer" data-label="sainsburys"  onClick="ga('send', 'event', { eventCategory: 'Link', eventAction: 'Click Link', eventLabel: 'Sainsburys'});"><img src="/images/where-to-buy/sainsburys.svg" class="logo animated fadeIn" data-id="4"/></a>
	</div>
	<div class="columns span-4 md-6 sm-6">
		<a href="https://www.superdrug.com/search?text=orajel" target="_blank" class="retailer" data-label="superdrug" onClick="ga('send', 'event', { eventCategory: 'Link', eventAction: 'Click Link', eventLabel: 'Superdrug'});"><img src="/images/where-to-buy/superdrug.svg" class="logo animated fadeIn" data-id="5"/></a>
	</div>
	{{--
	<div class="columns span-4 md-6 sm-6">
		<a href="http://www.well.co.uk/Pharmacy-Finder/" target="_blank" class="retailer" data-label="well" onClick="ga('send', 'event', { eventCategory: 'Link', eventAction: 'Click Link', eventLabel: 'Well'});"><img src="/images/where-to-buy/well-pharmacy.svg" class="logo animated fadeIn" data-id="6"/></a>
	</div>
	--}}
	<div class="columns span-4 md-6 sm-6">
		<a href="http://www.lloydspharmacy.com/SearchDisplay?categoryId=&storeId=10151&catalogId=10152&langId=44&sType=SimpleSearch&resultCatEntryType=2&showResultsPage=true&searchSource=Q&pageView=&beginIndex=0&pageSize=12&searchTerm=orajel#first" target="_blank" class="retailer" data-label="well" onClick="ga('send', 'event', { eventCategory: 'Link', eventAction: 'Click Link', eventLabel: 'Lloyds Pharmacy'});"><img src="/images/where-to-buy/lloyds-pharmacy.svg" class="logo animated fadeIn" data-id="6"/></a>
	</div>
	<div class="columns span-4 md-6 sm-6" style="padding-top:4em;">
		<a href="https://www.chemist-4-u.com/catalogsearch/result/?dir=desc&q=orajel" target="_blank" class="retailer" data-label="well" onClick="ga('send', 'event', { eventCategory: 'Link', eventAction: 'Click Link', eventLabel: 'Chemist 4 U'});"><img src="/images/where-to-buy/chemist4u.svg" class="logo animated fadeIn" data-id="6"/></a>
	</div>
	<div class="columns span-4 md-6 sm-6">
		<a href="https://groceries.morrisons.com/webshop/product/Orajel-Dental-Gel/380476011?from=search&tags=%7C105651&param=orajel&parentContainer=SEARCHorajel" target="_blank" class="retailer" data-label="well" onClick="ga('send', 'event', { eventCategory: 'Link', eventAction: 'Click Link', eventLabel: 'Morrisons'});"><img src="/images/where-to-buy/morrisons.svg" class="logo animated fadeIn" data-id="6"/></a>
	</div>
	<div class="columns span-12">
		<p class="animated fadeIn mt0 mb2" data-id="7">Find your nearest pharmacy at <a href="https://www.yell.com/ucs/UcsSearchAction.do?keywords=pharmacies&location=United+Kingdom&scrambleSeed=583335858" target="_blank" onClick="ga('send', 'event', { eventCategory: 'Link', eventAction: 'Click Link', eventLabel: 'Yell'});"><img src="/images/where-to-buy/yell.svg" class="logo-inline"/></a></p>
	</div>
</div>