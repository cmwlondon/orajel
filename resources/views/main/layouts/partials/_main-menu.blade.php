<a href="/"><img src="/images/orajel-logo.png" class="logo"/></a>

<ul class="main-menu-v2">
	<li><a href="/where-to-buy">Where to buy</a></li>
	<li><span class="divider">|</span></li>
	<li><a href="/mouth-ulcer">Mouth Ulcer</a></li>
	<li><span class="divider">|</span></li>
	<li><a href="/toothache">Toothache</a></li>
	<li><span class="divider">|</span></li>
	<li><a href="/">Home</a></li>
</ul>


<div class="mobile-menu">
	<img src="/images/hamburger.svg" class="burger"/>
</div>

<div class="slide-menu bg-red">
	<img src="/images/close.svg" class="close"/>
	<ul>
		<li><a href="/">Home</a></li>
		<li><a href="/toothache">Toothache</a></li>
		<li><a href="/mouth-ulcer">Mouth Ulcer</a></li>
		<li><a href="/where-to-buy">Where to buy</a></li>
	</ul>
</div>