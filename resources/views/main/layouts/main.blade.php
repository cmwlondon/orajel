<!doctype html>
<html class="no-js" lang="en">
	<head>
<!-- OneTrust Cookies Consent Notice start for orajel.co.uk -->
<script src="https://cdn.cookielaw.org/scripttemplates/otSDKStub.js"  type="text/javascript" charset="UTF-8" data-domain-script="1e0b2b7b-154c-4123-b2e0-7e9436126a4e" ></script>
<script type="text/javascript">
function OptanonWrapper() { }
</script>
<!-- OneTrust Cookies Consent Notice end for orajel.co.uk -->
		<meta charset="utf-8" />
		<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1" />
		

		<title>{{{$meta['title']}}}</title>
		<meta name="description" content="{{{$meta['desc']}}}" />
		<meta name="robots" content="INDEX, FOLLOW" />
		
		<link href='//fonts.googleapis.com/css?family=Open+Sans+Condensed%3A300&ver=1' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="/css/main/styles.css"/>
	
		@if (isset($pageViewCSS) && $pageViewCSS != '')
			<link rel="stylesheet" href="/css/{{{$pageViewCSS}}}.css"/>
		@endif

		<link rel="canonical" href="{{{$meta['link']}}}" />
		<meta property="og:locale" content="en_GB" />
		<meta property="og:type" content="{{{$meta['pagetype']}}}" />
		<meta property="og:title" content="{{{$meta['title']}}}" />
		<meta property="og:description" content="{{{$meta['desc']}}}" />
		<meta property="og:url" content="{{{$meta['link']}}}" />
		<meta property="og:site_name" content="Orajel" />
		<meta property="og:image" content="{{{$meta['image']}}}" />

		{{-- @include('main.layouts.partials._favicon') --}}
		@if (App::environment() == 'production' || App::environment() == 'test')
		<script async src="https://www.googletagmanager.com/gtag/js?id=AW-951730613"></script>
		<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){
			dataLayer.push(arguments);
		};
		gtag('js', new Date());
		gtag('config', 'AW-951730613');
		</script>
		@endif
		@yield('header')
	</head>
	
	<body>
		<a name="top"></a>
{{--
		<div class="cookie-disclaimer">To give you the best possible experience this site uses cookies. By continuing to use this website you are giving consent to cookies being used. For more information visit our <a href="/cookie-notice">Cookie&nbsp;Notice</a>.<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 64.6 64.6" enable-background="new 0 0 64.6 64.6" xml:space="preserve" class="btn-close">
			    <line x1="3.5" y1="3.5" x2="61.1" y2="61.1"/>
			    <line x1="61.1" y1="3.5" x2="3.5" y2="61.1"/>
			</svg></div>
--}}		
		<div id="wrapper">
			<div id="mobile-white-out" class="hidden"></div>
			<div id="headerWrapper">
				@include('main.layouts.partials._main-menu')
			</div>
			<div id="contentWrapper" class="{{{ $class_section }}}">
				<a name="content-section"></a>
				@yield('content')
			</div>
			<div id="footerWrapper">
				@yield('footer')
			</div>
		</div>
		<script>
			var site_url = "{{ $site_url }}";
			@if (isset($jsVars))
				@foreach ($jsVars as $key => $var)
					var {{$key}} = "{{ $var }}";
				@endforeach
			@endif
			@if (isset($pageViewJS))
				var pageViewJS = "{{ $pageViewJS }}";
			@endif		
		</script>

		<script data-main="/js/main/main.min.js?1" src="/js/libs/requirejs/require.js"></script>

		@if (App::environment() == 'production' || App::environment() == 'test')

		<!-- Google Analytics -->
		<script type="text/plain" class="optanon-category-2">

		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-31949095-1']);
		  _gaq.push(['_trackPageview']);

		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();

		</script>
		@endif
	</body>
</html>
