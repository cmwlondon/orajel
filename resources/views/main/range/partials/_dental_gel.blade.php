<h2 class="blue animated fadeIn" data-id="1">ORAJEL&trade; DENTAL GEL</h2>
<p class="animated fadeIn" data-id="2">Orajel&trade; Dental Gel delivers rapid relief when and where you need it most. The 10% w/w benzocaine is ideal to reduce pain associated with a broken tooth or in a tooth that may require a filling until you can see the dentist.</p>
<p class="animated fadeIn" data-id="3">Orajel&trade; Dental Gel form gives you control over the product during application and goes right to the source of your pain to provide targeted relief.</p>
<p class="animated fadeIn" data-id="4">Orajel&trade; Dental Gel contains benzocaine, a powerful local anaesthetic that works by temporarily blocking the pathway of pain signals along the nerves and relieves pain by numbing the area.</p>
