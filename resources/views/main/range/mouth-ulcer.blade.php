@extends('main.layouts.main')


@section('header')
	
@endsection


@section('content')
	<div class="block-blue">
		<div class="clear-menu">
			<h1>Mouth ulcer causes, symptoms and triggers</h1>
			<div class="row full">
				<div class="columns span-5 after-1 md-12 md-after-0 sm-12 sm-after-0">
					<h3>Causes</h3>
					<p>Mouth ulcers are a common condition. They are painful, round yellow or white sores that can appear as a single ulcer or as a cluster on the inside of the lips, cheeks, gums or on the tongue.</p>
					<p>The majority of mouth ulcers are defined as 'minor' and most are caused by trauma to the mouth, for example biting the inside of the cheek or from excessive brushing. Some people suffer from recurrent mouth ulcers. The causes of these are often unknown, but can be triggered by certain factors such as trauma to the mouth, stress and anxiety, hormonal changes or eating and drinking certain foods.</p>
					<p>Most mouth ulcers can heal without medication, but they often last for 10 to 14 days and can be painful.</p>
				</div>
				<div class="columns span-5 after-1 md-12 md-after-0 sm-12 sm-after-0">
					<h3>Symptoms</h3>
					<p>If you have a painful sore inside your mouth check to see if it's an ulcer by looking for these symptoms:</p>
					<ul class="white-list">
						<li>Mouth ulcers begin as a small bump or red spot inside the mouth.</li>
						<li>Sometimes a tingling or burning sensation signals a developing mouth ulcer.</li>
						<li>They progress rapidly to clearly defined spots in the mouth.</li>
						<li>They are small, but can be painful and can cause irritation.</li>
						<li>Mouth ulcers are typically small, however, occasionally one can be very large (2 or more inches) and can take as long as 6 weeks to fully heal.</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="block-blue spaced">
		<h3 class="mt2">Triggers</h3>
		<div class="row nopad mb3 animatedParent animateOnce" data-sequence="100">
			<div class="columns span-3 lg-6 md-12 sm-12 block eq">
				<div class="eq-inner">
					<img src="/images/ranges/lower-your-stress.png" class="ico animated fadeIn" data-id="1"/>
					<h4 class="blue">Lower your stress</h4>
					<p>Fatigue and emotional stress are commonly associated with the appearance of mouth ulcers. Make sure you get plenty of rest and relaxation.</p>
				</div>
			</div>
			<div class="columns span-3 lg-6 md-12 sm-12 block eq">
				<div class="eq-inner">
					<img src="/images/ranges/chew-carefully.png" class="ico animated fadeIn" data-id="2"/>
					<h4 class="blue">Chew carefully</h4>
					<p>Chew carefully to avoid small mouth injuries. If you bite the inside of the cheek or tongue, it can make you more prone to mouth ulcers.</p>
				</div>
			</div>
			<div class="columns span-3 lg-6 md-12 sm-12 block eq">
				<div class="eq-inner">
					<img src="/images/ranges/watch-what-you-eat.png" class="ico animated fadeIn" data-id="3"/>
					<h4 class="blue">Watch what you eat</h4>
					<p>Watch what you eat. Once a mouth ulcer starts avoid hot, spicy and acidic foods and drinks. These can irritate the mouth ulcer and make it more painful.</p>
				</div>
			</div>
			<div class="columns span-3 lg-6 md-12 sm-12 block eq">
				<div class="eq-inner">
					<img src="/images/ranges/treat-it.png" class="ico animated fadeIn" data-id="4"/>
					<h4 class="blue">Treat it</h4>
					<p>If you can&rsquo;t avoid a mouth ulcer, you can still treat it. Orajel™ Mouth Gel provides temporary pain relief for mouth ulcers and from wearing dentures. Always read the instructions carefully when using this product.</p>
				</div>
			</div>
		</div>
	</div>

	<div class="block-blue spaced">
		<a name="mouth-gel"></a>
		<h3>Ultimate Soother</h3>
	</div>

	<div class="block-grey">
		<div class="row nopad info mt3 animatedParent animateOnce" data-sequence="100">
			<div class="columns span-6 md-10 md-before-1 md-after-1 sm-10 sm-before-1 sm-after-1">
				@include('main.range.partials._mouth_gel')
			</div>
			<div class="columns span-6 md-12 sm-12 tac mb4">
				<img src="/images/ranges/mouth-gel.png" class="pack animated growIn" data-id="5" alt="Orajel Mouth Gel"/>
				<a href="#mouth-gel-usage" class="wtb btnUsage fancybox">Usage</a>
				<a href="/where-to-buy" class="wtb btnWtb btnBlue">Where to buy</a>
			</div>
		</div>
	</div>

	<div style="display:none">
		<div id="mouth-gel-usage" class="usage">
			<p class="mt0">Adults and children over 12 years.</p>
			<p>Directions:</p>
			<ul>
				<li>Clean and dry the affected area.</li>
				<li>Cut off tip of tube.</li>
				<li>Apply a thin layer of Orajel&trade; Mouth Gel to the areas inside the mouth which are tender or painful.</li>
				<li>Use up to 4 times daily, for no longer than 4 days.</li>
				<li>If tenderness or pain persists, consult your dentist or doctor.</li>
      			<li>Not for use on children below the age of 12 years.</li>
			</ul>
		</div>
	</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
