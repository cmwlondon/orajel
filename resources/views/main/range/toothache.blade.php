@extends('main.layouts.main')


@section('header')
	
@endsection


@section('content')
	<div class="block-red">
		<div class="clear-menu">
			<h1>Toothache causes, symptoms and triggers</h1>
			<div class="row full">
				<div class="columns span-5 after-1 md-12 md-after-0 sm-12 sm-after-0">
					<h3>Causes</h3>
					<p>Toothache occurs when dental pulp (the middle layer of the tooth, containing sensitive nerves and blood vessels) becomes exposed, irritated or inflamed. This can be caused by a number of factors, including dental decay, a fracture to the tooth, receding gums or certain dental treatments, such as a loose filling.</p>
				</div>
				<div class="columns span-5 after-1 md-12 md-after-0 sm-12 sm-after-0">
					<h3>Symptoms</h3>
					<p>The primary symptom of toothache is pain in your tooth and mouth, however the pain can occur in many various ways:</p>
					<ul class="white-list">
						<li>Sharp, throbbing, or constant pain</li>
						<li>Tooth pain that only occurs when pressure is applied</li>
						<li>Swelling and painful gums</li>
						<li>A bad taste in the mouth (from an infection in a tooth)</li>
						<li>Fever or headache</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="block-red spaced">
		<h3 class="mt2">Triggers</h3>
		<div class="row nopad mb3 animatedParent animateOnce" data-sequence="100">
			<div class="columns span-3 lg-6 md-12 sm-12 block eq">
				<div class="eq-inner">
					<img src="/images/ranges/cold-drinks-or-food.png" class="ico animated fadeIn" data-id="1"/>
					<h4 class="red">Cold Drinks or Food</h4>
					<p>Eating or drinking can make the pain worse, especially if the food or drink is practically cold, such as ice cream or cold water.</p>
				</div>
			</div>
			<div class="columns span-3 lg-6 md-12 sm-12 block eq">
				<div class="eq-inner">
					<img src="/images/ranges/sweet-foods.png" class="ico animated fadeIn" data-id="2"/>
					<h4 class="red">Sweet Foods</h4>
					<p>Limit your intake of sugary foods and drinks. Although they taste great, food and drink containing a high sugar content should be monitored and kept as a treat as too much can lead to tooth decay. You should also brush regularly using a toothpaste containing fluoride to minimise damage.</p>
				</div>
			</div>
			<div class="columns span-3 lg-6 md-12 sm-12 block eq">
				<div class="eq-inner">
					<img src="/images/ranges/biting-down.png" class="ico animated fadeIn" data-id="3"/>
					<h4 class="red">Biting Down</h4>
					<p>When a tooth is pushed down into the socket, due to an external or un-natural force such as an injury, it can result in toothache.</p>
				</div>
			</div>
			<div class="columns span-3 lg-6 md-12 sm-12 block eq">
				<div class="eq-inner">
					<img src="/images/ranges/other-pressure.png" class="ico animated fadeIn" data-id="4"/>
					<h4 class="red">Other Pressure</h4>
					<p>Tooth pain may be sharp, throbbing, or constant. In some people, pain occurs only when pressure is applied to the tooth.</p>
					
				</div>
			</div>
			<div class="columns span-12">
				<p>Anyone who experiences toothache should seek advice from a dentist.</p>
				<p class="mb0">For more information about toothache please visit the <a href="http://www.nhs.uk/conditions/Toothache/pages/introduction.aspx" target="_blank" rel="nofollow" class="underline">NHS website</a></p>
			</div>
		</div>
	</div>

	<div class="block-red spaced">
		<a name="dental-gel"></a>
		<h3>Emergency Tooth Saviour</h3>
	</div>

	<div class="block-grey">
		<div class="row nopad info mt3 animatedParent animateOnce dentalGel2" data-sequence="100">
			<div class="columns span-6 md-10 md-before-1 md-after-1 sm-10 sm-before-1 sm-after-1">
				@include('main.range.partials._dental_gel')
				<div class="buttons1" style="float:right;">
					<a href="#extra-strength-usage" class="wtb btnUsage fancybox">Usage</a>
					<a href="/where-to-buy" class="wtb btnWtb btnRed">Where to buy</a>
				</div>
			</div>
			<div class="columns span-6 md-12 sm-12 tac mb4 packshotBox">
				<img src="/images/ranges/dental-gel2.png" class="pack animated growIn" data-id="5" alt="Orajel Dental Gel" />
				<div class="buttons2">
					<a href="#extra-strength-usage" class="wtb btnUsage fancybox">Usage</a>
					<a href="/where-to-buy" class="wtb btnWtb btnRed">Where to buy</a>
				</div>
			</div>
		</div>
	</div>

	<div style="display:none">
		<div id="dental-gel-usage" class="usage">
			<p class="mt0">Adults and children over 12 years.</p>
			<p>Directions:</p>
			<ul>
				<li>Remove cap.</li>
				<li>Cut off tip of tube.</li>
				<li>With a clean finger or swab apply an amount of gel the size of a green pea into the tooth cavity.</li>
				<li>Use up to 4 times daily.</li>
				<li>Do not use continuously. If toothache persists, consult your dentist.</li>
				<li>Not for use in children below the age of 12 years.</li>
			</ul>
		</div>
	</div>


	<div class="block-yellow spaced">
		<a name="extra-strength"></a>
		<h3>Super Strength Relief</h3>
	</div>

	<div class="block-grey">
		<div class="row nopad info mt3 animatedParent animateOnce" data-sequence="100">
			<div class="columns span-6 md-10 md-before-1 md-after-1 sm-10 sm-before-1 sm-after-1">
				@include('main.range.partials._extra_strength')
			</div>
			<div class="columns span-6 md-12 sm-12 tac mb4">
				<img src="/images/ranges/extra-strength.png" class="pack animated growIn" data-id="5" alt="Orajel Extra Strength"/>
				<a href="#extra-strength-usage" class="wtb btnUsage fancybox">Usage</a>
				<a href="/where-to-buy" class="wtb btnWtb btnRed">Where to buy</a>
			</div>
		</div>
	</div>

	<div style="display:none">
		<div id="extra-strength-usage" class="usage">
			<p class="mt0">Adults and children over 12 years.</p>
			<p>Directions:</p>
			<ul>
				<li>Remove cap.</li>
				<li>Cut off tip of tube.</li>
				<li>With a clean finger or swab apply an amount of gel the size of a green pea into the tooth cavity.</li>
				<li>Use up to 4 times daily.</li>
				<li>Do not use continuously. If toothache persists, consult your dentist.</li>
				<li>Not for use in children below the age of 12 years.</li>
			</ul>
		</div>
	</div>

@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
