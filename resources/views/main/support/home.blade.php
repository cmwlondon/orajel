@extends('main.layouts.main')


@section('header')
	
@endsection


@section('content')

	<div class="block-red">
		<div class="row clear-menu mb1">
			<div class="columns span-12">
				<h3>CONTACT US</h3>
				<p>If you have a comment to share with us or question about Orajel then please complete our enquiry form below.</p>
				<p>If you get any side effects, talk to your doctor, pharmacist or nurse. This includes any possible side effects not listed in this leaflet. You can also report side effects directly via the Yellow Card Scheme at <a href="http://www.mhra.gov.uk/yellowcard" targte="_blank" rel="nofollow" class="underline">www.mhra.gov.uk/yellowcard</a>. By reporting side effects, you can help provide more information on the safety of this medicine.</p>
			</div>
			<div class="columns span-8 after-1 sm-12 md-12">
				
				<div class="form">
					@if (session('response'))
						<div class="formRow">
							<h3>{{ session('response.message') }}</h3>
						</div>
					@else

					<!-- composer, config/app: laravelcollective/html -->
					{!! Form::open(array('url' => ['contact-us'], 'method' => 'POST', 'id' => 'supportForm') )!!}

					<div class="formRow">
						{!! Form::label('title', 'Title: *') !!}
						{!! Form::select('title', $titlesArr, null, ['id' => 'title', 'class' => 'input short'] ) !!}
						{!! $errors->first('title', '<small class="error">:message</small>') !!}
					</div>

					<div class="formRow">
						{!! Form::label('firstname', 'First Name: *') !!}
						{!! Form::text('firstname',null,['placeholder' => '', 'id' => 'firstname', 'class' => 'input']) !!}
						{!! $errors->first('firstname', '<small class="error">:message</small>') !!}
					</div>

					<div class="formRow">
						{!! Form::label('lastname', 'Last Name: *') !!}
						{!! Form::text('lastname',null,['placeholder' => '', 'id' => 'lastname', 'class' => 'input']) !!}
						{!! $errors->first('lastname', '<small class="error">:message</small>') !!}
					</div>

					{{-- <div class="formRow">
						{!! Form::label('company', 'Company:') !!}
						{!! Form::text('company',null,['placeholder' => '', 'id' => 'company', 'class' => 'input']) !!}
						{!! $errors->first('company', '<small class="error">:message</small>') !!}
					</div> --}}

					{{-- <div class="formRow">
						{!! Form::label('address', 'Address:') !!}
						{!! Form::text('address',null,['placeholder' => '', 'id' => 'address', 'class' => 'input']) !!}
						{!! $errors->first('address', '<small class="error">:message</small>') !!}
					</div> --}}

					{{-- <div class="formRow">
						{!! Form::label('city', 'City:') !!}
						{!! Form::text('city',null,['placeholder' => '', 'id' => 'city', 'class' => 'input']) !!}
						{!! $errors->first('city', '<small class="error">:message</small>') !!}
					</div> --}}

					{{--<div class="formRow">
						{!! Form::label('county', 'County:') !!}
						{!! Form::text('county',null,['placeholder' => '', 'id' => 'county', 'class' => 'input']) !!}
						{!! $errors->first('county', '<small class="error">:message</small>') !!}
					</div>--}}

					{{--<<div class="formRow">
						{!! Form::label('postcode', 'Postcode:') !!}
						{!! Form::text('postcode',null,['placeholder' => '', 'id' => 'postcode', 'class' => 'input']) !!}
						{!! $errors->first('postcode', '<small class="error">:message</small>') !!}
					</div> --}}

					{{--<<div class="formRow">
						{!! Form::label('country', 'Country:') !!}
						{!! Form::text('country',null,['placeholder' => '', 'id' => 'country', 'class' => 'input']) !!}
						{!! $errors->first('country', '<small class="error">:message</small>') !!}
					</div> --}}

					<div class="formRow">
						{!! Form::label('phone', 'Phone Number: *') !!}
						{!! Form::text('phone',null,['placeholder' => '', 'id' => 'phone', 'class' => 'input']) !!}
						{!! $errors->first('phone', '<small class="error">:message</small>') !!}
					</div>

					<div class="formRow">
						{!! Form::label('email', 'Email Address: *') !!}
						{!! Form::text('email',null,['placeholder' => '', 'id' => 'email', 'class' => 'input']) !!}
						{!! $errors->first('email', '<small class="error">:message</small>') !!}
					</div>

					<div class="formRow">
						{!! Form::label('product', 'Product:') !!}
						{!! Form::text('product',null,['placeholder' => '', 'id' => 'product', 'class' => 'input']) !!}
						{!! $errors->first('product', '<small class="error">:message</small>') !!}
					</div>

					<div class="formRow">
						{!! Form::label('comments', 'Comments:') !!}
						{!! Form::textarea('comments',null,['placeholder' => '', 'id' => 'comments', 'rows' => '4', 'class' => 'input']) !!}
						{!! $errors->first('comments', '<small class="error">:message</small>') !!}
					</div>
<div class="formRow googleRecaptcha">
@if(env('GOOGLE_RECAPTCHA_KEY'))
     <div class="g-recaptcha"
          data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}">
     </div>
@endif
{!! $errors->first('g-recaptcha-response', '<small class="error">:message</small>') !!}
</div>

					<div class="formRow tar">
						{{-- {!! Form::submit('Submit', array('class' => 'btn-submit', 'id' => 'bSubmit', 'onClick' => "ga('send', 'event', { eventCategory: 'Contact', eventAction: 'Submit', eventLabel: 'Contact Form'});")) !!} --}}
						{!! Form::submit('Submit', array('class' => 'btn-submit', 'id' => 'bSubmit')) !!}
					</div>

					{!! Form::close() !!}


					@endif
				</div>

			</div>
			<div class="columns span-3 sm-12 md-12 media-enquiries">
				<p>Medical information Direct&nbsp;Line:<br>
				+44 800 028 1454</p>
				<p>Customer Care Direct&nbsp;Line:<br>
				+44 800 028 1454</p>
				<p>Sales enquiries only:<br>
				+44 1303 299 592</p>				
			</div>
		</div>
	</div>
	<script src='https://www.google.com/recaptcha/api.js'></script>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
