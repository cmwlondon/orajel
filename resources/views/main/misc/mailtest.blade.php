@extends('main.layouts.main')


@section('header')
	
@endsection


@section('content')
	<div class="block-red">
		<div class="clear-menu mb1" style="min-height:450px;">

			<h1>mailgun integration test</h1>
			<p>sending to hockey.richard@gmail.com</p>
			<p>time: {{{$time}}}</p>
			<p>recipient: {{{$recipient}}}</p>
			<p>ccrecipient: {{{$cc_recipient}}}</p>
		</div>
	</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
