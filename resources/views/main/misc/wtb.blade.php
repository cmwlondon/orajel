@extends('main.layouts.main')


@section('header')
@if (App::environment() == 'production' || App::environment() == 'test')
<script>
gtag('event', 'conversion', {'send_to': 'AW-951730613/F-izCP_Os50BELWD6cUD'});
</script>
@endif
@endsection


@section('content')
	<div class="block-red">
		<div class="clear-menu">
			<h1>Where to buy</h1>
			<div class="row full">
				<div class="columns span-12">
					<p>Orajel&trade; is available from Boots, Asda, Amazon, Sainsbury&rsquo;s, Superdrug, Lloyds Pharmacy, Morrisons and also widely available from a range of independent pharmacies.</p>
				</div>
			</div>

		</div>
	</div>

	<div class="block-grey where-to-buy mt2">
		<div class="row">
			@include('main.layouts.partials._wtb')
		</div>
	</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
