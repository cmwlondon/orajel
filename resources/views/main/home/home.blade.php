@extends('main.layouts.main')

@section('header')

@endsection


@section('content')
<div class="hero">
	<div class="desktop">
		<img alt="" src="/images/home/carousel/toothache-large-min.jpg" class="main">
		<a href="/toothache#dental-gel" title="Orajel dental gel"><img alt="Orajel dental gel" src="/images/home/carousel/pack-large.png" class="pack"></a>
		<a href="/toothache" class="wtb">FIND OUT MORE</a>
	</div>
	<div class="mobile">
		<img alt="" src="/images/home/carousel/toothache-large-min.jpg" class="main">
		<a href="/toothache#dental-gel" title="Orajel dental gel"><img alt="" src="/images/home/carousel/pack-large.png" class="pack"></a>
		<a href="/toothache" class="wtb">FIND OUT MORE</a>
	</div>
</div>

	{{-- <div class="carousel">
		<img src="/images/red-radial.jpg" class="shim"/>
		<div class="columns span-12 carousel-inner"></div>
		<ul class="carousel-control"></ul>
		
		@include('main.home.partials.image_map')
		
	</div> --}}

	<div class="block-soothes animatedParent animateOnce" data-sequence="100">
		<div class="left">
			<h1 class="animated fadeIn" data-id="1">Soothes Ulcer and denture pain fast</h1>
			<p>Orajel&trade; Mouth Gel provides effective relief from mouth ulcer and denture pain</p>
		</div>
		<div class="pack">
			<a href="/mouth-ulcer#mouth-gel"><img src="/images/home/mouth-gel.png" class="animated growIn" data-id="2"/></a>
			<p class="animated fadeIn" data-id="3">ORAJEL DENTAL GEL</p>
			<p class="animated fadeIn" data-id="4">BENZOCAINE 10% W/W</p>
		</div>
		<a href="/where-to-buy" class="wtb">Where to buy</a>
	</div>

	<div class="block-toothache eq">
		<div class="eq-inner animatedParent animateOnce" data-sequence="100">
			<img src="/images/home/toothache.png" class="animated fadeIn slowest" data-id="1"/>
			<div class="left">
				<h4 class="animated fadeIn" data-id="2">Toothache</h4>
				<h5 class="animated fadeIn" data-id="3">Symptoms &amp; Triggers</h5>
				<a href="/toothache" class="box animated fadeIn" data-id="4">Find out more about the signs and causes of toothache</a>
			</div>
			
		</div>
	</div>

	<div class="block-mouth-ulcer eq">
		<div class="eq-inner animatedParent animateOnce" data-sequence="100">
			<img src="/images/home/mouth-ulcer.png" class="animated fadeIn slowest" data-id="1"/>
			<div class="left">
				<h4 class="animated fadeIn" data-id="2">Mouth Ulcers &amp; Denture Pain</h4>
				<h5 class="animated fadeIn" data-id="3">Symptoms &amp; Triggers</h5>

				<a href="/mouth-ulcer" class="box animated fadeIn" data-id="4">Find out more about the signs and causes of mouth ulcers &amp; denture pain</a>
			</div>
			
		</div>

	</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection
