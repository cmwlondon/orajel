@extends('admin.layouts.main')

@section('content')

	@include('admin.layouts.partials.header')

	<div class="row">
		<div class="small-12 columns">
			<ul class="breadcrumbs">
			  <li><a href="/admin/blog">Blog</a></li>
			  <li class="current">Edit Post</li>
			</ul>

			{!! Form::model($post, array('url' => ['admin/blog/update/'.$post->id], 'method' => 'PUT', 'files' => true) )!!}
				<div class="row">
					<div class="small-12 columns mt1">
						@include('admin.blog.partials._form')
					</div>
				</div>
				<div class="row">
					<div class="small-12 columns mt1">
						{!! Form::submit('Update', array('class' => 'button success small')) !!}
						{!! link_to('admin/blog', 'Cancel', ['class' => 'small button alert']) !!}

						
					</div>
				</div>
				<input type="hidden" name="update" value="1"/>
			{!! Form::close() !!}
		</div>
		<div class="small-12 columns">
			<hr/>
			{!! Form::open(array('url' => 'admin/blog/destroy/'.$post->id, 'method' => 'delete', 'id' => 'del_'.$post->id)) !!}
		  		{!! link_to('#', 'Delete', ['onClick' => 'var r = confirm("Are you sure you want to delete this post?"); if (r == true) {document.getElementById("del_'.$post->id.'").submit();} return false;', 'class' => 'small button secondary rf']) !!}
		  	{!! Form::close() !!}
		</div>	
	</div>
@stop
