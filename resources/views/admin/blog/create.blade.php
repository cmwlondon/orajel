@extends('admin.layouts.main')

@section('content')

	@include('admin.layouts.partials.header')

	<div class="row">
		<div class="small-12 columns">
			<ul class="breadcrumbs">
			  <li><a href="/admin/blog">Blog</a></li>
			  <li class="current">Create Post</li>
			</ul>

			{!! Form::open(array('url' => ['admin/blog/store'], 'method' => 'POST', 'id' => 'blogForm', 'files' => true) )!!}

			@include('admin.blog.partials._form', ['type' => 'create', 'hidden' => 'hidden'])

			<div class="row">
				
				<div class="small-12 columns">
					<hr/>
					{!! Form::submit('Create', array('class' => 'button success small', 'id' => 'bSubmit')) !!}
					{!! link_to('admin/blog', 'Cancel', ['class' => 'small button alert']) !!}
				</div>
			</div>
			{!! Form::close() !!}

		</div>		
	</div>	
@stop
