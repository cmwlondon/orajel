@if (isset($response) && $response != '')
	<div class="row">
		<div class="small-12 columns">
			<div data-closable class="alert callout {{{ $response['response_status'] }}}">
  				{{{ $response['message'] }}}
  				<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
				    <span aria-hidden="true">&times;</span>
				  </button>
			</div>
		</div>
	</div>
@endif