<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

Route::get('/','Main\HomeController@index');

// RangeController
Route::get('/toothache','Main\RangeController@toothache');
Route::get('/mouth-ulcer','Main\RangeController@mouthUlcer');

// MiscController 
Route::get('/terms-&-conditions', 		'Main\MiscController@terms');
Route::get('/privacy-policy',			'Main\MiscController@privacy');
Route::get('/cookie-notice', 			'Main\MiscController@cookies');
Route::get('/where-to-buy', 			'Main\MiscController@wtb');
Route::get('/sitemap', 					'Main\MiscController@sitemap');

Route::get('/mailtest', 			'Main\MiscController@mailtest');

// SupportController
Route::get('/contact-us',				'Main\SupportController@index');
Route::post('/contact-us',				'Main\SupportController@store');


// sanitize support database, remove personal data in records older than 3 months
Route::get('/gdpr', function () {
    $exitCode = Artisan::call('gdpr', []);
    echo $exitCode;
});

/* */
// admin section
// DEFAULT LARAVEL 5.6.x authentication routes
// https://stackoverflow.com/questions/29183348/how-to-disable-registration-new-user-in-laravel-5
// /vendor/laravel/framework/src/Illuminate/Routing/Router.php

// Authentication Routes...
Auth::routes();
// -----------------------------------

// login
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');

// logut
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
// -----------------------------------
// disable guest registration
// $this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
// $this->post('register', 'Auth\RegisterController@register');

// route /register to login controller
Route::get('register', 'Auth\LoginController@showLoginForm')->name('register');

// -----------------------------------

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

// -----------------------------------

Route::group(['prefix' => 'admin'], function() {

	Route::get('/', function() {
		return redirect('admin/summary');
	});
	Route::get('summary', 'Admin\Summary\SummaryController@index');

	/*
	// changeed from model binding
	// Route::get('users', 'Admin\Users\UserController@index');
	Route::get('users', [ 'as' => 'admin.users.index', 'uses' => 'Admin\Users\UserController@index']);
	Route::get('users/create', [ 'as' => 'admin.users.create', 'uses' => 'Admin\Users\UserController@create']);
	Route::post('users/store', [ 'as' => 'admin.users.store', 'uses' => 'Admin\Users\UserController@store']);
	Route::get('users/edit/{id}', [ 'as' => 'admin.users.edit', 'uses' => 'Admin\Users\UserController@edit']);
	Route::put('users/update/{id}', [ 'as' => 'admin.users.update', 'uses' => 'Admin\Users\UserController@update']);
	Route::delete('users/destroy/{id}', [ 'as' => 'admin.users.destroy', 'uses' => 'Admin\Users\UserController@destroy']);
	*/

	// blog admin routes
	Route::get('blog', 'Admin\Blog\BlogController@index');
	Route::get('blog/create', 'Admin\Blog\BlogController@create');
	Route::post('blog/store', 'Admin\Blog\BlogController@store');
	Route::get('blog/edit/{id}', 'Admin\Blog\BlogController@edit');
	Route::put('blog/update/{id}', 'Admin\Blog\BlogController@update');
	Route::delete('blog/destroy/{id}', 'Admin\Blog\BlogController@destroy');

	// /sitemap/refresh this is the only part of the admin interface that actually does anything
	Route::get('sitemap/refresh', 'Admin\Sitemap\SitemapController@refresh');
});
