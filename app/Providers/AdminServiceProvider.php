<?php
namespace App\Providers;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\App;

class AdminServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        // App/Facades/AdminService facade bind name 'adminServiceFacade'
        App::bind('adminServiceFacade', function()
        {
            return new \App\Services\AdminService;
        });
    }
}
