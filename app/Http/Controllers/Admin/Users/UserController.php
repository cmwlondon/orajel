<?php namespace App\Http\Controllers\Admin\Users;

use Admin;
use App\Http\Controllers\Admin\AdminController;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\UserRequest as UserRequest;
use App\Models\User as User;
use App\Models\Roles;

class UserController extends AdminController {

	public function __construct(User $user) {
		
		parent::__construct();
		$this->middleware('auth');
		$this->user = $user;
	}

	public function index()
	{
		$this->context['title'] = 'User Management';
		
		$users = $this->user->get();

		$this->context['users'] = $users;	

		return view('admin.users.main', $this->context);
	}

	public function create()
	{
		$this->context['title'] = 'Create User';
		// get all roles for form 'Role' SELECT Field
		$this->context['roles'] = Roles::all();
		return view('admin.users.create', $this->context);

	}

	// POST on create form submission + validation
	public function store(UserRequest $request)
	{
		// echo "<pre>".print_r($request->all(), true)."</pre>";
		// die();

		$request['password'] = bcrypt($request->password);
		$this->user->fill($request->all());
		// save form data if passed validation rules
		$this->user->save();

		return redirect('admin/users')->with('response', $this->response);
	}

	public function show($id)
	{
		//
	}

	public function edit($id)
	{
		$user = $this->user->findOrFail($id);
		$this->context['title'] = 'User Management : Editing ' . $user->name;
		$this->context['user'] = $user;
		$this->context['roles'] = Roles::all();

		return view('admin.users.edit', $this->context);
	}

	public function update($id, UserRequest $request)
	{
		$user = $this->user->findOrFail($id);
		
		if($request->password != '') {
			$request['password'] = bcrypt($request->password);
		} else {
			unset($request['password']);
		}
		
		$user->update($request->all());
		
		$this->response = array('response_status' => 'success', 'message' => 'This user has been updated successfully.');
		
		return redirect('admin/users')->with('response', $this->response);
	}

	public function destroy($id)
	{	
		$user = $this->user->findOrFail($id);
		$user->delete();

		return redirect('admin/users');
	}
}
