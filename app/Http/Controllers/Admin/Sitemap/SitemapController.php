<?php namespace App\Http\Controllers\Admin\Sitemap;

use App\Http\Controllers\Admin\AdminController;
use App\Http\Requests;

use Request;

//use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;

use Config;
use Admin;

use App\Services\SitemapPackager;

class SitemapController extends AdminController {

	/**
	 * Create a new controller instance with dependency injection
	 *
	 * @return void
	 */
	public function __construct(Guard $guard, SitemapPackager $packager) {

		parent::__construct($guard);

		$this->packager = $packager;
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function refresh()
	{
		$pkg_response = $this->packager->create();
		return $pkg_response;

		/*
		$this->context['title'] = 'Sitemap refresh';
		return view('admin.sitemap.refresh', $this->context);
		*/
	}

}
