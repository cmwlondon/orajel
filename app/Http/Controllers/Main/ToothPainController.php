<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;

class ToothPainController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Show the application home screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{	
		$this->context['meta']['title']	= 'Orajel - Tooth Pain Is Hell';
		$this->context['meta']['desc'] = '';

		$this->context['pageViewJS']	= 'main/sections/tooth-pain.min';
		$this->context['pageViewCSS']	= 'main/sections/tooth-pain';

		$this->context['class_section'] = '';

		return view('main.tooth-pain.home', $this->context);
	}
}
