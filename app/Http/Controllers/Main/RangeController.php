<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;

class RangeController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	public function toothache()
	{	
		$this->context['meta']['title']	= 'Orajel - Toothache';
		$this->context['meta']['desc'] = '';

		$this->context['pageViewJS']	= 'main/sections/ranges.min';
		$this->context['pageViewCSS']	= 'main/sections/ranges';

		return view('main.range.toothache', $this->context);
	}

	public function mouthUlcer()
	{	
		$this->context['meta']['title']	= 'Orajel - Mouth Ulcers and Denture Pain';
		$this->context['meta']['desc'] = '';

		$this->context['pageViewJS']	= 'main/sections/ranges.min';
		$this->context['pageViewCSS']	= 'main/sections/ranges';

		return view('main.range.mouth-ulcer', $this->context);
	}
}
