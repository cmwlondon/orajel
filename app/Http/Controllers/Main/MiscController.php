<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;

use Mail;
use Response;
use Storage;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Services\SitemapPackager;

class MiscController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	public function about()
	{	
		$this->context['meta']['title']	= 'Orajel - About';
		$this->context['meta']['desc'] = '';

		$this->context['pageViewJS']	= ''; //'main/sections/home.min';
		$this->context['pageViewCSS']	= ''; //main/sections/home';

		return view('main.misc.about', $this->context);
	}

	public function wtb()
	{	
		$this->context['meta']['title']	= 'Orajel - Where To Buy';
		$this->context['meta']['desc'] = '';

		$this->context['pageViewJS']	= ''; //'main/sections/home.min';
		$this->context['pageViewCSS']	= ''; //main/sections/home';

		return view('main.misc.wtb', $this->context);
	}

	public function terms()
	{	
		$this->context['meta']['title']	= 'Orajel - Terms and Conditions';
		$this->context['meta']['desc'] = '';

		$this->context['pageViewJS']	= ''; //'main/sections/home.min';
		$this->context['pageViewCSS']	= ''; //main/sections/home';

		return view('main.misc.terms', $this->context);
	}

	public function cookies()
	{	
		$this->context['meta']['title']	= 'Orajel - Cookie Notice';
		$this->context['meta']['desc'] = '';

		$this->context['pageViewJS']	= ''; //'main/sections/home.min';
		$this->context['pageViewCSS']	= ''; //main/sections/home';

		return view('main.misc.cookies', $this->context);
	}

	public function privacy()
	{	
		$this->context['meta']['title']	= 'Orajel - Privacy Policy';
		$this->context['meta']['desc'] = '';

		$this->context['pageViewJS']	= ''; //'main/sections/home.min';
		$this->context['pageViewCSS']	= ''; //main/sections/home';

		return view('main.misc.privacy', $this->context);
	}

	public function thirdparty()
	{	
		$this->context['meta']['title']	= 'Orajel - Third Party Information Collection';
		$this->context['meta']['desc'] = '';

		$this->context['pageViewJS']	= ''; //'main/sections/home.min';
		$this->context['pageViewCSS']	= ''; //main/sections/home';

		return view('main.misc.third-party', $this->context);
	}

	public function sitemap(SitemapPackager $packager)
	{	

		// $response = Storage::disk('public')->get('sitemap.xml'); // /storage/app/public/sitemap.xml
		// return Response::make($response, '200')->header('Content-Type', 'text/xml');

		$pkg_response = $packager->create();
		return $pkg_response;

	}

	public function mailtest()
	{	
		$this->context['meta']['title']	= 'Orajel - mailgun integration test';
		$this->context['meta']['desc'] = '';

		$this->context['pageViewJS']	= ''; //'main/sections/home.min';
		$this->context['pageViewCSS']	= ''; //main/sections/home';

		/*
		Mail::raw('Here is a test message from the orajel contact us form via the MailGun test domain', function($message)
		{
			$message->subject('orajel + Mailgun sandbox domain');
			$message->from('orajel@sandbox677aa5c46b624036a28a2e15eb445c02.mailgun.org', 'orajel');
			$message->to('hockey.richard@gmail.com'); // address in list of authorised test addresses for sandbox
		});
		*/

		$recipient = env('MAIL_SYS_TO');
		$cc_recipient = env('MAIL_ADMIN_CC');

		$this->context['time'] = date("Y-m-d H:i:s");
		$this->context['recipient'] = $recipient;
		$this->context['cc_recipient'] = $cc_recipient;
		
		$details = array(
			'title'			=> 'Mr',
			'firstname'		=> 'Albert',
			'lastname'		=> 'Einstein',
			'phone'			=> '7542087301',
			'email'			=> 'albert.einstain@gmail.com',
			'product'		=> 'orajel mailgun test product',
			'comments'		=> 'orajel meilgun test comment',
			'mfgcode'		=> 'sentient.orajel.088',
			'timestamp' 	=> date("Y-m-d H:i:s"),
			'company'		=> '',
			'address'		=> '',
			'city'			=> '',
			'county'		=> '',
			'postcode'		=> '',
			'country'		=> ''
		);

		// /resources/views/emails/enquiry.blade.php
		/* */
		Mail::send(['text' => 'emails.enquiry'], $details, function( $message ) use ( $details )
		{
			$message->from('orajel-mail-test@orajel.co.uk', 'orajel');
			// $message->to('hockey.richard@gmail.com');
			$message->to( env('MAIL_SYS_TO') );
			$message->cc( env('MAIL_ADMIN_CC') );
		    $message->subject('Orajel mail test');
		});
		/* */
		return view('main.misc.mailtest', $this->context);
	}
}
