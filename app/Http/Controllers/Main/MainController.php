<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use Request;

class MainController extends Controller {
	
	public function __construct() {
		$this->context = array(
			'pageViewCSS' 		=> "",
			'pageViewJS'		=> "",
			'site_url'			=> url('',[]),
			'class_section'	=> '',
			'meta'				=> [
									'title' => 'Orajel',
									'link' => url(Request::path()),
									'pagetype' => 'article',
									'desc' => '',
									'image' => url('/images/orajel-logo.png')
									]
		);
	}
}