<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;

class HomeController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Show the application home screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{	
		$this->context['meta']['title']	= 'Orajel - Fast Effective Toothache Relief';
		$this->context['meta']['desc'] = '';

		$this->context['pageViewJS']	= 'main/sections/home.min';
		$this->context['pageViewCSS']	= 'main/sections/home';

		return view('main.home.home', $this->context);
	}
}
