<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;

class FightersController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Show the application home screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{	
		$this->context['meta']['title']	= 'Orajel - Finger Fighters';
		$this->context['meta']['desc'] = '';

		$this->context['pageViewJS']	= 'main/sections/finger-fighters.min';
		$this->context['pageViewCSS']	= 'main/sections/finger-fighters';

		$this->context['class_section'] = '';

		return view('main.fighters.home', $this->context);
	}
}
