<?php namespace App\Http\Requests\Main;

use Illuminate\Foundation\Http\FormRequest;
use Validator;
use Request;

class SupportRequest extends FormRequest {

	public function __construct() {

	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		// add validation rule to comments field: detect link tags in text
		// to prevent spam bots using the form
		/*
		(
		1460,
		'Mrs',
		'Аdult dаting оnline аmeriсаn singles: https://1borsa.com/datingsexygirlsinyourcity757452',
		'Аdult dаting оnline аmeriсаn singles: https://1borsa.com/datingsexygirlsinyourcity757452',
		'',
		'',
		'',
		'',
		'',
		'',
		'81711274794',
		'hejmiechristensen@gmail.com',
		'Thе best girls fоr sех in yоur town Сanаda: https://klurl.nl/?u=h1SbEwX3',
		'Adult #1 dаting aрр for iрhonе: https://onlineuniversalwork.com/datingsexygirlsinyourcity587846',
		'2020-01-30 11:32:32',
		'2020-01-30 11:32:32'
		);

		firstname, lastname, product, comments
		*/
		$rules = [
			'title'				=> 'required|max:255',
			'firstname'			=> 'required|max:255',
			'lastname'      	=> 'required|max:255',
			//'company'      		=> 'max:255',
			//'address'      		=> 'max:255',
			//'city'      		=> 'max:255',
			//'county'			=> 'max:255',
			//'postcode'			=> 'max:20',
			//'country'			=> 'max:255',
			'phone'				=> 'min:5|max:20',
			'email'				=> 'required|email',
			'product'			=> 'max:255',
			'comments'			=> '',
			'g-recaptcha-response' => 'required|recaptcha'
		];

		return $rules;
	}

	public function messages()
	{
	    return [
	    	'g-recaptcha-response.required' => 'Please complete the captcha'
	    ];
	}

	public function withValidator($validator)
	{
		$validator->after(function ($validator)
		{
			$fails = ['http','www','http://','https://','http%3A%2F%2F','https%3A%2F%2F'];
			$spamfields = ['firstname','lastname','phone','product','comments'];

			foreach ( $spamfields AS $spamfield )
			{
				foreach ( $fails AS $fail )	
				{
					if (strpos($this->$spamfield,$fail) !== FALSE)
					{
						$validator->errors()->add($spamfield, 'No URLs please.');
					}
				}
			}
		});
	}

}
